﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MusicoreLib
{
    public class Config
    {
        public string FileExtensions { get; set; }

        public string Path { get; set; }

        public string XmlPath { get; set; }
        public string ItunesLibPath { get; set; }

        public Config()
        {
            FileExtensions = "*.mp3";
            Path = @"C:\Users\Theo\Downloads\";//Frank Zappa - Buffalo (2007)";
            XmlPath = "lib.xml";
            ItunesLibPath = @"C:\Users\Theo\Music\iTunes\iTunes Music Library.xml";
        }
    }
}
