﻿using System.Collections.Generic;

namespace MusicoreLib
{
    public interface IArtist
    {
        string Hash { get; set; }
        string Name { get; set; }
        List<IAlbum> Albums { get; set; }
    }

    public class Artist : IArtist
    {
        public string Hash { get; set; }
        public string Name { get; set; }
        public List<IAlbum> Albums { get; set; }

        public Artist()
        {
            Albums = new List<IAlbum>();
        }

        public Artist(string name) : this()
        {
            Name = string.IsNullOrEmpty(name) ? "Unknown" : name;
            Hash = "A_" + HashProvider.HAsh(Name);
        }
    }
}