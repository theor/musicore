﻿using System.Collections.Generic;
using System.IO;

namespace MusicoreLib
{
    public class Folder
    {
        public string Name { get { return Path.GetDirectoryName(FullPath); } }
        public string FullPath { get; set; }
        public List<Folder> Children { get; set; }
        public List<Song> Songs { get; set; }

        public Folder()
        {
            Children = new List<Folder>();
            Songs = new List<Song>();
        }

        public Folder(string path) : this()
        {
            FullPath = path;
        }
    }
}