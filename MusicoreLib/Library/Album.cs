﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace MusicoreLib
{
    public interface IAlbum
    {
        string Hash { get; set; }

        [XmlIgnore]
        IArtist Artist { get; set; }

        string Name { get; set; }
        List<ISong> Songs { get; set; }
    }

    public class Album : IAlbum
    {
        public string Hash { get; set; }
        [XmlIgnore]
        public IArtist Artist { get; set; }
        public string Name { get; set; }
        public List<ISong> Songs { get; set; }

        public Album(IArtist artist, string name) : this()
        {
            Artist = artist;
            if (string.IsNullOrEmpty(name))
            {
                Name = "Unknown";
                Hash = "a_" + HashProvider.HAsh(Artist.Name + Name);
            }
            else
            {
                Name = name;
                Hash = "a_" + HashProvider.HAsh(Name);
            }

            
        }

        public Album()
        {
             Songs = new List<ISong>();
        }
    }
}