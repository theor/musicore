﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using MusicoreLib.Library;
using File = TagLib.File;

namespace MusicoreLib
{
    public static class HashProvider
    {
        private static SHA1Cng _provider = new SHA1Cng();
        static HashProvider()
        {
            _provider.Initialize();
        }
        public static string HAsh(string s)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(s ?? "");
            string hash = BitConverter.ToString(
                _provider.ComputeHash(buffer)).Replace("-", "");

            return hash;
        }
    }

    public interface ISong
    {
        [XmlIgnore]
        IArtist Artist { get; set; }

        [XmlIgnore]
        IAlbum Album { get; set; }

        string Title { get; set; }
        string FullPath { get; set; }
        string Hash { get; set; }
        string FileName { get; }
    }

    public class Song : ISong
    {
        [XmlIgnore]
        public IArtist Artist { get; set; }
        [XmlIgnore]
        public IAlbum Album { get; set; }
        public string Title { get; set; }
        public string FullPath { get; set; }
        public string Hash { get; set; }

        public string FileName { get { return Path.GetFileName(FullPath); } }


        [XmlIgnore]
        public File Taglib { get; set; }

        public void ExtractTags(ILib lib)
        {
            File songFile = File.Create(new File.LocalFileAbstraction(FullPath));
            Taglib = songFile;
            Title = string.IsNullOrEmpty(songFile.Tag.Title) ? Path.GetFileName(FullPath) : songFile.Tag.Title;
            string firstPerformer = Taglib.Tag.FirstPerformer;
            if (string.IsNullOrEmpty(firstPerformer))
                firstPerformer = "Unknown";
            Artist = lib.Artists.FirstOrDefault(x => x.Name == firstPerformer);
            if (Artist == null)
            {
                Artist = new Artist(firstPerformer);
                lib.Artists.Add(Artist);
            }

            string album = Taglib.Tag.Album;
            if (String.IsNullOrEmpty(album))
                album = "Unknown";
            Album = Artist.Albums.FirstOrDefault(x => x.Name == album);
            if (Album == null)
            {
                Album = new Album(Artist, album);
                Artist.Albums.Add(Album);
            }
            Album.Songs.Add(this);
        }

        public Song()
        {}

        public Song(string fullPath)
        {
            FullPath = fullPath;
            Hash = HashProvider.HAsh(fullPath);
        }

        public Song(Artist artist, Album album, string title)
        {
            Artist = artist;
            Album = album;
            Title = title;
        }

        public override string ToString()
        {
            if (Taglib == null)
                return FileName;
            return String.Format("{0} - {1}", Taglib.Tag.FirstPerformer, Taglib.Tag.Title);
        }
    }
}