﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MusicoreLib.Player;

namespace MusicoreLib.Library
{
    public class Playlist
    {
        private readonly IPlayer player_;
        private readonly List<ISong> songs_;
        private ISong current_;

        public Playlist(IPlayer player)
        {
            player_ = player;
            songs_ = new List<ISong>();
        }

        public virtual List<ISong> Songs
        {
            get { return songs_; }
        }

        public virtual void Enqueue(ISong s)
        {
            songs_.Add(s);
        }


        public virtual void Play(ISong s)
        {
            if (songs_.Count == 0)
                return;
            current_ = s;
            PlaySong();
        }

        private void PlaySong()
        {
            Action<ZPlayer.ICallbackArgs> callback = e =>
                                             {
                                                 if(e.Stop)
                                                    Next();
                                             };
            player_.Play(current_, callback);
        }

        public virtual ISong Current()
        {
            return current_;
        }

        public virtual void Prev()
        {
            if(current_ == null)
                return;
            int i = songs_.IndexOf(current_);
            if (i == -1)
                return;
            current_ = songs_[(i - 1 + songs_.Count) % songs_.Count];
            PlaySong();
        }

        public virtual void Next()
        {
            if (current_ == null)
                return;
            int i = songs_.IndexOf(current_);
            if(i == -1)
                return;
            current_ = songs_[(i + 1)%songs_.Count];
            PlaySong();
        }

        public virtual void Move(bool up, ISong s)
        {
            // 0 1 2 3
            // a b c d

            // 0 1 2
            // a b d

            // move down
            // 0 1 2 3
            // a b d c
            // move up
            // 0 1 2 3
            // a c b d



            int i = Songs.IndexOf(s);
            if(i == -1)
                throw new Exception(s.Title + "is not in playlist");
            if(up && i == 0 || (!up) && i == Songs.Count - 1)
                return;
            Songs.RemoveAt(i);
            i = up ? i - 1 : i + 1;
            Songs.Insert(i, s);
        }

        public virtual void Delete(ISong s)
        {
            if(!Songs.Contains(s))
                throw new Exception(s.Title + "is not in playlist");
            Songs.Remove(s);

        }

        public ISong GetSong(string hash)
        {
            return Songs.FirstOrDefault(s => s.Hash == hash);
        }
    }
}
