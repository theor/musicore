﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using File = System.IO.File;

namespace MusicoreLib.Library
{
    public class DeserializableAttribute : Attribute
    {
        
    }

    public abstract class ILib
    {
        protected Config Config;
        public List<IArtist> Artists { get; set; }
        public IEnumerable<IGrouping<char, IArtist>> ArtistsLetters { get; protected set; }
        public abstract void Save();
        public abstract void PostLoad();
        public virtual void LoadConfig(Config c)
        {
            Config = c;
        }

        public static T Load<T>(string file) where T : ILib, new()
        {
            T lib;
            if (typeof(T).GetCustomAttributes(typeof(DeserializableAttribute), true).Count() > 0)
                lib = MainXmlSerializer.Deserialize<T>(file);
            else
                lib = new T();
            lib.PostLoad();
            return lib;
        }

        public abstract bool Exists(Config c);
        public ISong FindSong(string hash)
        {
            return Artists.SelectMany(a => a.Albums.SelectMany(ab => ab.Songs)).FirstOrDefault(s => s.Hash == hash);
        }

        public IArtist FindArtist(string hash)
        {
            return Artists.FirstOrDefault(s => s.Hash == hash);
        }

        public IAlbum FindAlbum(string hash)
        {
            return Artists.SelectMany(a => a.Albums).FirstOrDefault(s => s.Hash == hash);
        }
    }

    public class ItunesLib : ILib
    {
        public ItunesLib()
        {
            Artists = new List<IArtist>();
        }

        public override void Save()
        {
            throw new NotImplementedException();
        }

        public override void PostLoad()
        {
            
        }

        public override void LoadConfig(Config c)
        {
            base.LoadConfig(c);
            if(!File.Exists(c.ItunesLibPath))
                throw  new Exception(c.ItunesLibPath);

            var root = XDocument.Load(c.ItunesLibPath);

            var xElement = root.Root.Element("dict");
            var xsongs = from s in xElement.Element("dict").Elements("dict")
                        select s;
            //var xsong = xsongs.First();
           // var songs = new Song(x.Elements("key").First(k => k.Value == "Location").n.Value))
            
            foreach (var xsong in xsongs)
                HandleSong(xsong);
            Artists = Artists.OrderBy(a => a.Name).ToList();
            ArtistsLetters = Artists.GroupBy(a => a.Name.ToUpper()[0]);

        }

        private void HandleSong(XElement first)
        {
            Dictionary<string, string> d = new Dictionary<string, string>();
            string last = "";
            foreach (var element in first.Elements())
            {
                if (element.Name == "key")
                    last = element.Value;
                else
                    d.Add(last, element.Value);
            }
            ISong song =
                new Song(
                    first.Elements("key").FirstOrDefault(k => k.Value == "Location").ElementsAfterSelf().First().Value);

            song.Hash = d["Track ID"];

            string x = null;

            if (!d.TryGetValue("Name", out x))
                x = song.FileName;
            song.Title = x;

            if (!d.TryGetValue("Artist", out x))
                x = "Unknown";
            string x1 = x;
            song.Artist = Artists.FirstOrDefault(a => a.Name.ToLower() == x1.ToLower());
            if (song.Artist == null)
                Artists.Add(song.Artist = new Artist(x1));

            if (!d.TryGetValue("Album", out x))
                x = "";
            song.Album = song.Artist.Albums.FirstOrDefault(a => a.Name.ToLower() == x.ToLower());
            if (song.Album == null)
                song.Artist.Albums.Add(song.Album = new Album(song.Artist, x));
            song.Album.Songs.Add(song);
        }

        public override bool Exists(Config c)
        {
            return File.Exists(c.ItunesLibPath);
        }
    }

    [Deserializable]
    public class Lib : ILib
    {
        [XmlIgnore]
        public Container Container { get; set; }

        public Folder Root { get; set; }

        public Lib()
        {
            Artists = new List<IArtist>();
            Container = new Container();
        }

        public override void LoadConfig(Config config)
        {
            base.LoadConfig(config);
            Root = new Folder(config.Path);

            FindFiles();
            ExtractTags();
            Save();


        }

        public override bool Exists(Config c)
        {
            return File.Exists(c.XmlPath);
        }

        public void FindFiles()
        {
            var stack = new Stack<Folder>();
            stack.Push(Root);

            while (stack.Count != 0)
            {
                var f = stack.Pop();

                foreach (var file in Directory.EnumerateFiles(f.FullPath, Config.FileExtensions))
                    f.Songs.Add(new Song(file));
                foreach (var subPath in Directory.EnumerateDirectories(f.FullPath))
                {
                    Folder sub = new Folder(subPath);
                    f.Children.Add(sub);
                    stack.Push(sub);
                }
            }
        }

        private void ExtractTagsInFolder(Folder folder)
        {
            foreach (var song in folder.Songs)
                song.ExtractTags(this);
            foreach (var child in folder.Children)
                ExtractTagsInFolder(child);
        }

        public void ExtractTags()
        {
            if (Root == null)
                throw new Exception("Root null");
            ExtractTagsInFolder(Root);
        }

        public override void Save()
        {
            MainXmlSerializer.Serialize(this, Config.XmlPath);
        }

        public override void PostLoad()
        {
            foreach (var artist in Artists)
            {
                foreach (var album in artist.Albums)
                {
                    album.Artist = artist;
                    foreach (var song in album.Songs)
                    {
                        song.Album = album;
                        song.Artist = artist;
                    }
                }
            }
        }
    }
}
