﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace MusicoreLib
{
    public class MainXmlSerializer
    {
        private static readonly Dictionary<String, XmlSerializer> Serializers = new Dictionary<String, XmlSerializer>();

        public static void Serialize<T>(T obj, string file)
        {
            var ser = GetOrCreateSerializer<T>();
            using (var tw = new StreamWriter(file))
            using (var x = new XmlTextWriter(tw){Formatting = Formatting.Indented})
                ser.Serialize(x, obj);
        }

        private static XmlSerializer GetOrCreateSerializer<T>()
        {
            XmlSerializer ser;
            Type xmlTypeMapping = typeof(T);

            if (Serializers.TryGetValue(xmlTypeMapping.FullName, out ser))
                return ser;
            ser = new XmlSerializer(xmlTypeMapping);
            Serializers.Add(xmlTypeMapping.FullName, ser);
            return ser;
        }

        public static T Deserialize<T>(string file) where T:class 
        {
            var ser = GetOrCreateSerializer<T>();
            try
            {
                using (var tw = new StreamReader(file))
                using (var x = new XmlTextReader(tw))
                    return (T) ser.Deserialize(x);
            }
            catch (Exception)
            {
                if(Debugger.IsAttached)
                    Debugger.Break();
            return null;
            }

        }
    }
}
