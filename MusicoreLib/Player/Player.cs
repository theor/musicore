﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MusicoreLib.Player
{

    public enum VolumeModifier
    {
        Down = -1,
        Mute = 0,
        Up = 1
    }

    public interface IPlayer : IDisposable
    {
        void PlayPause();
        void Play(ISong s, Action<ZPlayer.ICallbackArgs> callback = null);
        int Volume { get; set; }
        int SetVolume(VolumeModifier modifier);
    }

    //public class NAudioPlayer : IPlayer
    //{
    //    IWavePlayer waveOutDevice;
    //    WaveStream mainOutputStream;
    //    WaveChannel32 volumeStream;

    //    private Action<ZPlayer.ICallbackArgs> callback_;

    //    public NAudioPlayer()
    //    {
    //        waveOutDevice = new WasapiOut(AudioClientShareMode.Shared, 100);
    //    }

    //    void PlaybackStopped(object sender, ZPlayer.ICallbackArgs e)
    //    {
    //        if (callback_ != null)
    //            callback_(e);
    //    }
        
       

    //    public void Dispose()
    //    {
    //        CloseWaveOut();
    //    }

    //    public void Play(ISong s, Action<ZPlayer.ICallbackArgs> callback)
    //    {
    //        if(mainOutputStream != null)
    //        {
    //            mainOutputStream.Dispose();
    //        }

    //        mainOutputStream = CreateInputStream(s.FullPath);

    //        //waveOutDevice.PlaybackStopped -= PlaybackStopped;
    //        try
    //        {
    //            waveOutDevice.Init(mainOutputStream);
    //        }
    //        catch (Exception e)
    //        {
    //            Console.WriteLine(e);
    //        }
    //       // waveOutDevice.PlaybackStopped += PlaybackStopped;
    //        waveOutDevice.Play();
    //        callback_ = callback;
    //    }

    //    public float Volume
    //    {
    //        get { return waveOutDevice.Volume; }
    //        set { waveOutDevice.Volume = value; }
    //    }

    //    public void PlayPause()
    //    {
    //        if(waveOutDevice == null)
    //            return;
    //        switch (waveOutDevice.PlaybackState)
    //        {
    //            case PlaybackState.Playing:
    //                waveOutDevice.Pause();
    //                break;
    //            case PlaybackState.Paused:
    //                waveOutDevice.Play();
    //                break;
    //            default:
    //                break;
    //        }
    //    }

    //    private WaveStream CreateInputStream(string fileName)
    //    {
    //        Uri uri = new Uri(fileName);

    //        WaveChannel32 inputStream;
    //        if (fileName.EndsWith(".mp3"))
    //        {
    //            var mp3FileName = Uri.UnescapeDataString(uri.AbsolutePath).Replace("/", "\\");
    //            if (mp3FileName.StartsWith("\\"))
    //                mp3FileName = mp3FileName.Substring(1);
    //            WaveStream mp3Reader = new Mp3FileReader(mp3FileName);
    //            inputStream = new WaveChannel32(mp3Reader);
    //        }
    //        else
    //        {
    //            throw new InvalidOperationException("Unsupported extension");
    //        }
    //        volumeStream = inputStream;
    //        return volumeStream;
    //    }

    //    private void CloseWaveOut()
    //    {
    //        if (waveOutDevice != null)
    //        {
    //            waveOutDevice.Stop();
    //        }
    //        if (mainOutputStream != null)
    //        {
    //            // this one really closes the file and ACM conversion
    //            volumeStream.Close();
    //            volumeStream = null;
    //            // this one does the metering stream
    //            mainOutputStream.Close();
    //            mainOutputStream = null;
    //        }
    //        if (waveOutDevice != null)
    //        {
    //            waveOutDevice.Dispose();
    //            waveOutDevice = null;
    //        }
    //    }
    //}

    //public class IrrklangPlayer : IPlayer
    //{
    //    private readonly ISoundEngine irrKlangEngine_;
    //    private ISound currentlyPlayingSound_;

    //    public IrrklangPlayer()
    //    {
    //        irrKlangEngine_ = new ISoundEngine();
    //    }

    //    public void Play(ISong s)
    //    {
    //        if (currentlyPlayingSound_ != null)
    //            currentlyPlayingSound_.Stop();
    //        if(!File.Exists(s.FullPath))
    //            throw new FileNotFoundException(s.FullPath);
    //        var src = irrKlangEngine_.AddSoundSourceFromFile(@"C:\Users\Mo\Downloads\Frank Zappa - Buffalo (2007)\Disc 2\01 - Easy Meat.mp3");//s.FullPath);

    //        currentlyPlayingSound_ = irrKlangEngine_.Play2D(s.FullPath);
            
    //        if(currentlyPlayingSound_ != null)
    //            currentlyPlayingSound_.Paused = false;
    //    }

    //    public void PlayPause()
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public void Dispose()
    //    {
    //        //if(currentlyPlayingSound_ != null)
    //        //    currentlyPlayingSound_.Dispose();
    //        //irrKlangEngine_.Dispose();
    //    }
    //}
}
