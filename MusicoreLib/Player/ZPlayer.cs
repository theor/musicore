﻿using System;
using System.Security.Policy;
using libZPlay;

namespace MusicoreLib.Player
{
    public class ZPlayer : IPlayer
    {
        public interface ICallbackArgs
        {
            bool Stop { get; }
            bool Play { get; }
        }
        public class CallbackArgs : EventArgs, ICallbackArgs
        {
            public TCallbackMessage Msg { get; set; }

            public CallbackArgs(TCallbackMessage msg)
            {
                Msg = msg;
            }

            public bool Stop
            {
                get { return Msg == TCallbackMessage.MsgStop || Msg == TCallbackMessage.MsgStopAsync; }
            }

            public bool Play
            {
                get { return Msg == TCallbackMessage.MsgPlay || Msg == TCallbackMessage.MsgPlayAsync; }
            }
        }

        private readonly ZPlay player_;
        private Action<ICallbackArgs> callback_;
        private TCallbackFunc callbackFunc_;

        public ZPlayer()
        {
            player_ = new ZPlay();
            callbackFunc_ = new TCallbackFunc(Callback);
            const TCallbackMessage messages = TCallbackMessage.MsgStopAsync | TCallbackMessage.MsgPlayAsync;
            if (!player_.SetCallbackFunc(callbackFunc_, messages, 0))
                throw new Exception(player_.GetError());
        }

        private int Callback(uint objptr, int userdata, TCallbackMessage msg, uint param1, uint param2)
        {
            if (callback_ != null)
                callback_(new CallbackArgs(msg));
            return 0;
        }

        public void Dispose()
        {
            player_.Close();
        }

        public void PlayPause()
        {
            var stat = new TStreamStatus();
            player_.GetStatus(ref stat);
            bool b = stat.fPlay ? player_.PausePlayback() : player_.ResumePlayback();
            if (!b)
                throw new Exception(player_.GetError());
        }

        public void Play(ISong s, Action<ICallbackArgs> callback)
        {
            callback_ = null;

            var path = s.FullPath;
            if (path.StartsWith("file://localhost/"))
                path = path.Remove(0, 17);
            path = Uri.UnescapeDataString(path);
            if (!player_.OpenFile(path, TStreamFormat.sfAutodetect))
                throw new Exception(player_.GetError());

            callback_ = callback;
            if (!player_.StartPlayback())
                throw new Exception(player_.GetError());

        }

        public int Volume
        {
            get
            {
                int v = 0;
                player_.GetPlayerVolume(ref v, ref v);
                return v;
            }
            set
            {
                if (!player_.SetPlayerVolume(value, value))
                    throw new Exception(player_.GetError());

            }
        }

        private int _mutedVolume;

        public int SetVolume(VolumeModifier modifier)
        {
            const int delta = 10;
            var v = Volume;
            switch (modifier)
            {
                case VolumeModifier.Down:
                    if(v - delta >= 0)
                        v -= delta;
                    break;
                case VolumeModifier.Mute:
                    if(_mutedVolume == 0)
                    {
                        _mutedVolume = v;
                        v = 0;
                    }
                    else
                    {
                        v = _mutedVolume;
                        _mutedVolume = 0;
                    }
                    break;
                case VolumeModifier.Up:
                    if (v + delta <= 100) 
                        v += delta;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("modifier");
            }
            Volume = v;
            return v;
        }
    }
}