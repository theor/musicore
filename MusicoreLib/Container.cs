﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq;
using System.Text;

namespace MusicoreLib
{
    public class Container : CompositionContainer
    {
        private static Container _instance;
        public static Container Get
        {
            get
            {
                if (_instance == null) throw new Exception("container not composed");
                return _instance;
            }
        }

        public Container(params ExportProvider[] providers) : base(providers)
        {
            _instance = this;
        }
    }
}
