﻿Ext.regController('Home', {
 
    // index action
    index: function()
    {
        if ( ! this.indexView)
        {
            this.indexView = this.render({
                xtype: 'HomeIndex',
            });
        }
 
        this.application.viewport.setActiveItem(this.indexView);
    },

    about: function()
    {
        if ( ! this.aboutView)
        {
            this.aboutView = this.render({
                xtype: 'HomeAbout',
            });
        }
 
        this.application.viewport.setActiveItem(this.aboutView);
    },

    browse: function(options)
    {
        if ( ! this.browseView)
        {
            this.browseView = this.render({
                xtype: 'HomeBrowse',
            });
        }
		var id = options.treeId || 'root';
		App.stores.tree.load({params: {node: id}});
        this.application.viewport.setActiveItem(this.browseView);
    },
});