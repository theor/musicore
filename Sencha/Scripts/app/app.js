﻿


Ext.regApplication({
    name: 'App',
    defaultUrl: 'Home/index',
    launch: function () {
        this.viewport = new Ext.ux.Viewport();
    },
});

Ext.regModel('Item', {
	fields: ['id', 'label', 'count', 'leaf',]
});

Ext.regModel('Song', {
	fields: ['id', 'title', 'artist', 'album', 'lenght']
});


App.callUrl = 'http://192.168.1.117/Musicore/';
App.stores.tree = new Ext.data.JsonStore({
	model: 'Item',
	proxy: {
		type: 'scripttag',
		url: App.callUrl+'Home/Tree',
		reader: {
			type: 'tree',
			root: 'children'
		}
	}
});
App.stores.currentPl = new Ext.data.JsonStore({
	model: 'Song',
	proxy: {
		type: 'scripttag',
		url: App.callUrl + 'Home/CurrentPlaylist',
		pageParam: undefined,
		reader: {
			type: 'json',
			root: 'songs'
		}
	}
});
