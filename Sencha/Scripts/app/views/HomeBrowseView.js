﻿App.views.HomeBrowse = Ext.extend(Ext.ux.BufferedList, {
	id: 'HomeBrowseView',
    itemTpl: '{label}',
    maxItemHeight: 75, // must specify
    store: App.stores.tree,
	constructor: function(config){
		App.views.HomeBrowse.superclass.constructor.call(this, config);
		this.selectedId = undefined;
		this.songSheet = new Ext.ActionSheet({
			id: 'songSheet',
			items: [{
				text: 'Play',
				handler:function(){
					Ext.getCmp('songSheet').hide();
					Ext.util.JSONP.request({
						url: App.callUrl + 'Home/Play',
						callbackKey: 'callback',
						params: {songHash: Ext.getCmp('HomeBrowseView').selectedId},
						callback: function(result) {
							var r = result;
						}
					});
				}
			},{
				text: 'Enqueue',
				handler:function(){
					Ext.getCmp('songSheet').hide();
					Ext.util.JSONP.request({
						url: App.callUrl + 'Home/Enqueue',
						callbackKey: 'callback',
						params: {songHash: Ext.getCmp('HomeBrowseView').selectedId},
						callback: function(result) {
							var r = result;
						}
					});
				}
			},{
				text: 'Close', handler: function(){
					Ext.getCmp('songSheet').hide();
				}
			}]
		});
	},

    listeners: {
        itemtap: function (list, index, elt, e) {
            var item = list.store.getAt(index);
			if(item.data.leaf){
				 this.selectedId = item.data.id;
				this.songSheet.show();
			} else {
				var id = item.data.id;
				var url = 'Home/browse';
				if(id.charAt(0) == 'A')
					url = 'albums/' + id;
				if(id.charAt(0) == 'a')
					url = 'songs/' + id;	
				Ext.dispatch({
					controller: 'Home',
					action    : 'browse',
					historyUrl: url,
					treeId: id
				});
			}
        }
    }
});

Ext.reg('HomeBrowse', App.views.HomeBrowse);

//Ext.extend(Ext.Panel, {
//    html: '<h2>Browse</h2> <p>MvcTouch is a Sencha Touch demo application.</p>',
//    scroll: 'vertical',
//    styleHtmlContent: true,
//    style: 'background: #d8efed',
//});