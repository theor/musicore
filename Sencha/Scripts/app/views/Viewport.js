﻿var json = function (callUrl, params, callBack) {
    //return function(){
    if (params == undefined)
        params = {};
    Ext.util.JSONP.request({
        url: App.callUrl+callUrl,
        callbackKey: 'callback',
        params: params,
        callback: callBack
    });
    //}
};

Ext.namespace('Ext.ux');

Ext.ux.Viewport = function (config){
	Ext.ux.Viewport.superclass.constructor.call(this, config);
	this.hideVolTask = new Ext.util.DelayedTask(function(){
		var slider = App.views.Viewport.query('#volSlider')[0];
		slider.hide();
	});
	this.hideVolTask.delay(1000);
};

Ext.ux.TabBar = function(config){
	Ext.ux.TabBar.superclass.constructor.call(this, config);
	//this.setActiveItem(1);
}

Ext.extend(Ext.ux.TabBar, Ext.TabBar, {
	activeItem: 1,
	dock : 'bottom',
	ui   : 'light',
	layout: {
		pack: 'center'
	},
	itemCreated: function(config){
		Ext.TabBar.superclass.constructor.call(this, config);
		this.setActiveItem(1);
	},
	listeners: {
		change: function(bar, tab, comp){
			bar.items.each(function(i){
				i.el.removeCls('x-tab-active');
			});
			tab.el.addCls('x-tab-active');
			Ext.dispatch({
					controller: tab.controller,
					action    : tab.action,
					historyUrl: tab.url || tab.controller + '/' + tab.action
				});
		}
	},
	items: [
		{
			text: 'Playing',
			iconCls: 'download',
			controller: 'Home',
			action:'index',
			url : 'playing'
		},
		{
			text: 'Browse',
			iconCls: 'bookmarks',
			controller: 'Home',
			action:'browse'
		},
		{
			text: 'About',
			iconCls: 'info',
			controller: 'Home',
			action:'about'
		}
	]
});

Ext.extend(Ext.ux.Viewport, Ext.Panel, {
    fullscreen: true,
    layout: 'card',
    cardSwitchAnimation: 'slide',
	tabBar: {
				dock: 'bottom',
				ui: 'light',
				layout: {
					pack: 'center'
				}
			},
	setVolume: function (v) {
		this.hideVolTask.delay(1000);
		var slider = App.views.Viewport.query('#volSlider')[0];
		slider.show({type:'slide', direction:'down'});
		slider.setValue(v.volume);
	},
	updateInfos: function(){
		Ext.util.JSONP.request({
			url: App.callUrl+'Home/SongInfos',
			callbackKey: 'callback',
			callback: function(song){
				var html = '';
				if(song.Error != undefined){
					html = song.Error;
				}
				else{
					html = '<div class="info">' + '<span class="title">' + song.title+'</span>' + '<span class="artist">' + song.artist + '</span>' + '</div>';
				}
				Ext.getCmp('info').update(html);
			}
		});
	},
    dockedItems: [
		{
		xtype: 'toolbar',
			defaults: {
				iconMask: true,
				ui: 'plain'
			},
			items: [{
				id:'log',
				text:'jklj'
				
			}]
		},
		{
			xtype: 'toolbar',
			defaults: {
				iconMask: true,
				ui: 'plain'
			},
			items: [{
				iconCls: 'iconPrev',
				handler: function () {
					Ext.getCmp('log').update('prev');
					json('Home/Prev');
					App.views.Viewport.updateInfos();
				}
			}, {
				iconCls: 'iconPause',
				handler: function () {
					json('Home/PlayPause');
					App.views.Viewport.updateInfos();
				}
			}, {
				iconCls: 'iconNext',
				handler: function () {
					json('Home/Next');
					App.views.Viewport.updateInfos();
				}
			},

				{
					iconCls: 'iconMute',
					handler: function () {
						json('Home/Volume', { mod: 0 }, function(v){
							App.views.Viewport.setVolume(v);
						});
					}
				}, {
					iconCls: 'iconVolDown',
					handler: function () {
						json('Home/Volume', { mod: -1 }, function(v){
							App.views.Viewport.setVolume(v);
						});
					}
				}, {
					iconCls: 'iconVolUp',
					handler: function () {
						json('Home/Volume', { mod: 1 }, function(v){
							App.views.Viewport.setVolume(v);
						});
					}
				},


			]
		},
		new Ext.ux.TabBar({
			
		}),
		// {
			// xtype: 'toolbar',
			// dock: 'bottom',
			
			// defaults: {
				// iconMask: true,
				// ui: 'light',
			// },
			// items: [{
				// iconCls: 'download',
				// handler: function () {
					// json('Home/Prev');
					// updateInfos();
				// }
			// }]
		// },
		new Ext.Panel({
		layout: 'fit',
			items: [
				new Ext.form.Slider({
					id: 'volSlider',
					label: 'Volume',
					//hidden: true,
					value: 50,
					minValue: 0,
					maxValue: 100,
					increment: 10,
					
				})
			]
		})
	]
});
//var buttons = ;

App.views.Viewport = new Ext.ux.Viewport();