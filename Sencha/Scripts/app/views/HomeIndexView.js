﻿App.views.HomeIndex = Ext.extend(Ext.Panel, {
	id: 'player',
	iconCls: 'download',
	title: 'Playing',
	updateInfos: function(){
		Ext.util.JSONP.request({
				url: 'Home/SongInfos',
				callbackKey: 'callback',
				callback: function(song){
					var html = '';
					if(song.Error != undefined){
						html = song.Error;
					}
					else{
						html = '<div class="info">' + '<span class="title">' + song.title+'</span>' + '<span class="artist">' + song.artist + '</span>' + '</div>';
					}
					Ext.getCmp('info').update(html);
				}
			});
	},
	listeners: {
		activate: function(){
			App.stores.currentPl.load();
			this.updateInfos();
		},
	},
	dockedItems:[
		new Ext.form.Slider({
			id: 'progress',
			value: 50,
			increment: 10,
			minValue: 0,
			maxValue: 100,
			dock: 'bottom'
		}),
	new Ext.Panel({
			id : 'info',
			html: 'test <em>test</em>',
			dock: 'bottom'
		}),
	],
	items:[
		new Ext.List({
			id: 'playing',
			title: 'Playing',
			

			itemTpl: '<em>{artist}</em> - {title}',
			store : App.stores.currentPl,

            listeners: {
                itemtap: function(list, index, elt, e){
                
                },
				menuoptiontap: function(data, record){
					var jsonParams = {songHash: record.data.id};
					var method =  data.id == 'play' ? 'Play' : 'Item';
					
					switch(data.id)
					{
						case 'delete':
							jsonParams.mod = 0;
						break;
						case 'move_up':
							jsonParams.mod = 1;
						break;
						case 'move_down':
							jsonParams.mod = 2;
						break;
					}
					Ext.util.JSONP.request({
						url: App.callUrl + 'Home/' + method,
						callbackKey: 'callback',
						params: jsonParams,
						callback: function(result) {
							App.stores.currentPl.load();
						}
					});
				}
            },
			
			plugins: [new Ext.ux.touch.ListOptions({
							hideOnScroll: true,
                            menuOptions: [{
                                id: 'play',
                                cls: 'option_play'
                            }, {
                                id: 'move_up',
                                cls: 'option_up'
                            }, {
                                id: 'move_down',
                                cls: 'option_down'
                            }, {
                                id: 'delete',
                                cls: 'option_delete'
                            }]
                        })],

			onItemDisclosure: function(record, btn, index) {
				Ext.util.JSONP.request({
				url: App.callUrl + 'Home/Play',
					
				callbackKey: 'callback',
				params: {songHash: record.data.id},
			});
			}
		}),
	],
});
Ext.reg('HomeIndex', App.views.HomeIndex);
