﻿Ext.Router.draw(function (map) {
	map.connect('albums/:treeId', {controller:'Home', action:'browse'});
	map.connect('songs/:treeId', {controller:'Home', action:'browse'});
	map.connect('playing', {controller:'Home', action:'index'});
    map.connect(':controller/:action');
});