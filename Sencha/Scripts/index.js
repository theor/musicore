﻿Musicore = new Ext.Application({
	name: "Musicore",

    launch: function() {
	
		var json = function(callUrl, params, callBack){
			//return function(){
				if(params == undefined)
					params = {};
				Ext.util.JSONP.request({
					url: callUrl,
					callbackKey: 'callback',
					params: params,
					callback: callBack
				});
			//}
		}
		
		var updateInfos = function(){
			Ext.util.JSONP.request({
					url: 'Home/SongInfos',
					callbackKey: 'callback',
					callback: function(song){
						var html = '';
						if(song.Error != undefined){
							html = song.Error;
						}
						else{
							html = '<div class="info">' + '<span class="title">' + song.title+'</span>' + '<span class="artist">' + song.artist + '</span>' + '</div>';
						}
						Ext.getCmp('info').update(html);
					}
				});
		}
		
		Musicore.SelectedSong = undefined;
	
		Ext.regModel('Item', {
			fields: ['id', 'label', 'count', 'leaf',]
		});

		Ext.regModel('Song', {
			fields: ['id', 'title', 'artist', 'album', 'lenght']
		});

		
		var nstore = new Ext.data.TreeStore({
			autoLoad: true,
            model: 'Item',
            proxy: {
                type: 'ajax',
                url: 'Home/Tree',
                reader: {
                    type: 'tree',
                    root: 'children'
                }
            }
        });

		var test = new Ext.NestedList({
			id: 'test',
            cls
			indexBar: true,
			iconCls: 'bookmarks',
			title: 'Browse',
			store: nstore,
            displayField: 'label',
			// add a / for folder nodes in title/back button
            getTitleTextTpl: function() {
                return '{' + this.displayField + '}<tpl if="leaf !== true">/</tpl>';
            },
            // add a / for folder nodes in the list
           getItemTextTpl: function() {
                return '{' + this.displayField + '}';
            },
            // provide a codebox for each source file
            getDetailCard: function(record, parentRecord) {
                return undefined;
            },
		});
	
		songSheet = new Ext.ActionSheet({
			items: [{
				text: 'Play',
				handler:function(){
					songSheet.hide();
					Ext.util.JSONP.request({
						url: 'Home/Play',
						callbackKey: 'callback',
						params: {songHash: Musicore.SelectedSong},
						callback: function(result) {
							var r = result;
						}
					});
				}
			},{
				text: 'Enqueue',
				handler:function(){
					songSheet.hide();
					Ext.util.JSONP.request({
						url: 'Home/Enqueue',
						callbackKey: 'callback',
						params: {songHash: Musicore.SelectedSong},
						callback: function(result) {
							var r = result;
						}
					});
				}
			},{
				text: 'Close', handler: function(){songSheet.hide();}
			}]
		});
	
		
	  test.on('leafitemtap', function(subList, subIdx, el, e, detailCard) {
            var ds = subList.getStore(),
                r  = ds.getAt(subIdx);
				Musicore.SelectedSong = r.node.id;
			songSheet.show();
			return;
            Ext.util.JSONP.request({
				url: 'Home/Play',
					
				callbackKey: 'callback',
				params: {songHash: r.node.id},
				callback: function(result) {
					var r = result;
				}
			});
        });
		
		var curplStore = new Ext.data.JsonStore({
			autoLoad: true,
			model: 'Song',
			proxy: {
				type: 'ajax',
				url: 'Home/CurrentPlaylist',
				pageParam: undefined,
				reader: {
					type: 'json',
					root: 'songs'
				}
			}
		});
				var player = new Ext.Panel({
					id: 'player',
					iconCls: 'download',
					title: 'Playing',
					listeners: {
								activate: function(){
									curplStore.load();
									updateInfos();
								},
							},
                    dockedItems:[
                        new Ext.form.Slider({
							id: 'progress',
							value: 50,
							increment: 10,
							minValue: 0,
							maxValue: 100,
                            dock: 'bottom'
						}),
                    new Ext.Panel({
							id : 'info',
							html: 'test <em>test</em>',
                            dock: 'bottom'
						}),
                    ],
					items:[
						new Ext.List({
							id: 'playing',
							title: 'Playing',
                            

							itemTpl: '<em>{artist}</em> - {title}',
							store : curplStore,
							onItemDisclosure: function(record, btn, index) {
								Ext.util.JSONP.request({
								url: 'Home/Play',
									
								callbackKey: 'callback',
								params: {songHash: record.data.hash},
							});
							}
						}),
					],
				});

	   var buttons = {
			xtype: 'toolbar',
			defaults: {
				iconMask: true,
				ui: 'plain'
			},
			items: [{
				    iconCls: 'iconPrev',
				    handler: function(){
					    json('Home/Prev')
					    updateInfos();
				    }
			    }, {
				    iconCls: 'iconPause',
				    handler: function(){
					    json('Home/PlayPause')
					    updateInfos();
				    }
			    }, {
				    iconCls: 'iconNext',
				    handler: function(){
					    json('Home/Next')
					    updateInfos();
                    }
			    },

				{
				    iconCls: 'iconMute',
				    handler: function(){
					    json('Home/PlayPause')
					    updateInfos();
				    }
			    },{
				    iconCls: 'iconVolDown',
				    handler: function(){
					    json('Home/PlayPause')
					    updateInfos();
				    }
			    },{
				    iconCls: 'iconVolUp',
				    handler: function(){
					    json('Home/PlayPause')
					    updateInfos();
				    }
			    },
                    
                
			]
		};

		Musicore.Viewport = new Ext.TabPanel({
			fullscreen: true,
			id: 'content',
			cardSwitchAnimation: 'slide',
			items: [player, test],//, list],
			dockedItems: [buttons],
			tabBar: {
				dock: 'bottom',
				ui: 'light',
				layout: {
					pack: 'center'
				}
			},
		});
	}

});