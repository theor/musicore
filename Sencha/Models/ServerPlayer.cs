﻿using System.IO;
using MusicoreLib;
using MusicoreLib.Library;
using MusicoreLib.Player;

namespace Sencha.Models
{
    public class ServerPlayer
    {
        public static ServerPlayer Instance = new ServerPlayer();

        public ServerPlayer()
        {
            Player = new ZPlayer();
            Config = new Config();


            if (File.Exists(Config.ItunesLibPath))
                Lib = ItunesLib.Load<ItunesLib>(Config.ItunesLibPath);
            else
                Lib = new ItunesLib();
            Lib.LoadConfig(Config);


            DefaultPlaylist = new Playlist(Player);
        }

        public IPlayer Player { get; set; }
        public ILib Lib { get; set; }
        public Config Config { get; set; }
        public Playlist DefaultPlaylist { get; private set; }
    }
}