﻿using MusicoreLib;

namespace Sencha.Controllers
{
    public class TreeModel
    {
        public string id { get; set; }
        public string label { get; set; }
        public int count { get; set; }
        public bool leaf { get; set; }

        private TreeModel(string id, string label, int count = -1, bool leaf = false)
        {
            this.id = id;
            this.label = label;
            this.count = count;
            this.leaf = leaf;
        }

        public TreeModel(ISong s)
            : this(s.Hash, s.Title, -1, true)
        { }

        public TreeModel(IArtist a)
            : this(a.Hash, a.Name, a.Albums.Count)
        { }

        public TreeModel(IAlbum a)
            : this(a.Hash, a.Name, a.Songs.Count)
        { }
    }
}