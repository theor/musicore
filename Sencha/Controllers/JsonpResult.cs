﻿using System;
using System.Web.Mvc;

namespace Sencha.Controllers
{
    public class JsonpResult : JsonResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            var request = context.HttpContext.Request;
            var response = context.HttpContext.Response;
            //string jsoncallback = (context.RouteData.Values["jsoncallback"] as string) ?? request["jsoncallback"];
            string jsoncallback = request.QueryString["callback"];
            if (!string.IsNullOrEmpty(jsoncallback))
            {
                if (string.IsNullOrEmpty(ContentType))
                {
                    ContentType = "application/x-javascript";
                }
                response.Write(string.Format("{0}(", jsoncallback));
            }
            base.ExecuteResult(context);
            if (!string.IsNullOrEmpty(jsoncallback))
            {
                response.Write(")");
            }
        }

        public JsonpResult(object data, JsonRequestBehavior behavior = JsonRequestBehavior.AllowGet)
        {
            Data = data;
            JsonRequestBehavior = behavior;
        }
    }
}