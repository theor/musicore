﻿using MusicoreLib;

namespace Sencha.Controllers
{
    public class SongModel
    {
        public string id { get; set; }
        public string title { get; set; }
        public string artist { get; set; }
        public string album { get; set; }
        public string length { get; set; }

        public SongModel(string id, string title, string artist, string album)
        {
            this.id = id;
            this.title = title;
            this.artist = artist;
            this.album = album;
            length = "1:23";
        }

        public SongModel(ISong x)
            : this(x.Hash, x.Title, x.Artist.Name, x.Album.Name)
        {
        }
    }
}