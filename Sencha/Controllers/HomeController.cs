﻿using System;
using System.Linq;
using System.Web.Mvc;
using MusicoreLib;
using MusicoreLib.Library;
using MusicoreLib.Player;
using Sencha.Models;

namespace Sencha.Controllers
{
    [NoCache]
    public class HomeController : Controller
    {
        private static ServerPlayer Sp { get { return ServerPlayer.Instance; } }

        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CurrentPlaylist()
        {
            var data = Sp.DefaultPlaylist.Songs.Select(x => new SongModel(x));
            return new JsonpResult(new { songs = data });
        }

        public ActionResult Enqueue(string songHash)
        {
            var s = Sp.Lib.FindSong(songHash);
            Sp.DefaultPlaylist.Enqueue(s);
            return new JsonpResult(s.Title);
        }

        public class ErrorModel
        {
            public ErrorModel(string error)
            {
                Error = error;
            }

            public string Error { get; set; }
        }

        public ActionResult Item(ItemModifier mod, string songHash)
        {
            ISong s = Sp.DefaultPlaylist.GetSong(songHash);
            if (s == null)
                return new JsonpResult(new ErrorModel("Song not found"));
            switch (mod)
            {
                case ItemModifier.Del:
                    Sp.DefaultPlaylist.Delete(s);
                    break;
                case ItemModifier.Up:
                    Sp.DefaultPlaylist.Move(true, s);
                    break;
                case ItemModifier.Down:
                    Sp.DefaultPlaylist.Move(false, s);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("mod");
            }

            return SongInfos();
        }
       

        public ActionResult Play(string songHash)
        {
            ISong song = Sp.Lib.FindSong(songHash);
            if (song != null)
            {
                //ServerPlayer.Instance.Player.Play(song);
                if (!Sp.DefaultPlaylist.Songs.Contains(song))
                    Sp.DefaultPlaylist.Enqueue(song);
                Sp.DefaultPlaylist.Play(song);

                return SongInfos();
            }
            return new JsonpResult(new ErrorModel("no such song " + songHash));
        }

        public ActionResult PlayPause()
        {
            ServerPlayer.Instance.Player.PlayPause();
            return SongInfos();
        }

        public ActionResult Volume(VolumeModifier mod)
        {
            float v = Sp.Player.SetVolume(mod);
            return new JsonpResult(new {volume = v});
        }

        public ActionResult Next()
        {
            Sp.DefaultPlaylist.Next();
            return SongInfos();
        }

        public ActionResult Prev()
        {
            Sp.DefaultPlaylist.Prev();
            return SongInfos();
        }

        public ActionResult SongInfos()
        {
            ISong current = Sp.DefaultPlaylist.Current();
            if (current == null)
                return new JsonpResult(new ErrorModel("no song selected"));
            return new JsonpResult(new SongModel(current));
        }

        public ActionResult Tree(object _dc, string node)
        {
            if (node == null)
                return null;
            ILib lib = ServerPlayer.Instance.Lib;
            object children = null;
            if (node == "root")
                children = lib.Artists.Select(a => new TreeModel(a));
            else
            {
                char kind = node[0];
                //string hash = node[0].Substring(2);
                switch (kind)
                {
                    case 'A':
                        IArtist ar = lib.FindArtist(node);
                        children = ar.Albums.Select(a => new TreeModel(a));
                        break;
                    case 'a':
                        IAlbum ab = lib.FindAlbum(node);
                        children = ab.Songs.Select(a => new TreeModel(a));
                        break;
                    default:
                        break;
                }
            }
            return new JsonpResult(new { text = "text", children });

        }

    }

    public enum ItemModifier
    {
        Del = 0,
        Up = 1,
        Down = 2,
    }
}
