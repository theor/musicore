﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MusicoreLib;
using MusicoreLib.Library;
using MusicoreLib.Player;

namespace MusicoreConsole
{
    class Program
    {
        //[STAThread]
        static void Main(string[] args)
        {
            Config c = new Config();
         //   Lib lib = new Lib(c);//, @"C:\Music\Opeth\The Roundhouse Tapes (disc 1)");
            //LibHelper.Fill(lib, c,@"C:\Users\Mo\Downloads\Frank Zappa - Buffalo (2007)");
            //lib.FindFiles();
            //Print(lib.Root);
            //lib.ExtractTags();
            //Print(lib);

            //var p = new NAudioPlayer();
            //p.Play(lib.Artists[0].Albums[0].Songs[0]);
            

            Console.ReadLine();
        }

        static string GetIndent(int i = 0)
        {
            StringBuilder sb = new StringBuilder(i);
            for (int j = 0; j < i; j++)
                sb.Append(" ");
            return sb.ToString();
        }

        static void Print(Folder folder, int indent = 0)
        {
            var ind = GetIndent(indent);
            Console.WriteLine(ind + folder.Name);
            foreach (var song in folder.Songs)
                Console.WriteLine(ind + "  " + song);
            foreach (var child in folder.Children)
            {
                Print(child, indent + 2);
            }
        }

        //static void Print(Lib lib)
        //{
        //    foreach (var artist in lib.Artists)
        //    {
        //        Console.WriteLine(artist.Name);
        //        foreach (var album in artist.Albums)
        //        {
        //            Console.WriteLine("  " + album.Name);
        //            foreach (var song in album.Songs)
        //                Console.WriteLine("    " + song.Title);
        //        }
        //    }
        //}
    }
}
